#!/bin/bash
#
# Usage: ./run.sh config.yaml
#

export PYTHONPATH=$PYTHONPATH:$(pwd)
python3 --version
python3 -B tgbots/main.py $1
