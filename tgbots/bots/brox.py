import re
import random

import telepot
import telepot.async

import unicodedata

import tgbots.util


class BroX(tgbots.util.Bot):
    def __init__(self, config, persistance):
        super(BroX, self).__init__(config, persistance)

    def brox(self, text):
        # normalize
        text = unicodedata.normalize('NFKD', text)

        # convert to ascii
        text = text.encode('ASCII', 'ignore').decode('UTF-8')

        # process
        text = text.lower()
        text = re.sub('ss', 's', text)
        text = re.sub(' o ', ' u ', text)
        text = re.sub('^o ', 'u ', text)
        text = re.sub(r'a\b', 'ah', text)
        text = re.sub(r'\bc', 'k', text)
        text = text.replace('s', 'x')

        text.replace('para o', 'pro')

        output = ''

        # markov chain
        state = True  # False = lower; True = upper
        switchprob = 0.3

        for char in text:
            if random.random() < switchprob:
                state = not state

            if char == 's':
                char = random.choice(['s', 'x'])

            if state:
                output += char.upper()
            else:
                output += char

        return output

    async def on_inline_query(self, msg):
        query_id, from_id, text = telepot.glance(
            msg, flavor='inline_query')

        brox = self.brox(text)

        if len(brox) == 0:
            return

        reply = {
            'id': 'brox',
            'type': 'article',
            'title': brox,
            'message_text': brox
        }

        await self.answerInlineQuery(
            query_id,
            [reply],
            cache_time=1)

    def on_chosen_inline_result(self, msg):
        pass

    async def on_chat_message(self, msg):
        content_type, chat_type, chat_id = telepot.glance(msg)

        if chat_type == 'private':
            await self.sendMessage(
                chat_id,
                parse_mode='Markdown',
                text='This is an inline bot. To use it, type\n' +
                '`@%s <text>`\n' % self.me['username'] +
                'and wait a few moments.')

__bot__ = BroX
