import telepot
import telepot.async
import logging

import asyncio
import tgbots.util

import pydle
from tornado.platform.asyncio import AsyncIOMainLoop

logger = logging.getLogger(__name__)


class IRCRelay(tgbots.util.Bot):
    class IRCClient(pydle.Client):
        def on_connect(self):
            super().on_connect()

            for channel in self.config.get('channels', []):
                self.join(channel)

        @pydle.coroutine
        def on_message(self, target, source, message):
            super().on_message(target, source, message)

            for chat in self.config.get('chats', []):
                yield from self.bot.sendMessage(
                    chat,
                    '*%s*\n%s' % (source, message),
                    parse_mode='Markdown')

    def __init__(self, config, persistance):
        super(IRCRelay, self).__init__(config, persistance)

        AsyncIOMainLoop().install()
        loop = asyncio.get_event_loop()

        self.chats = {}
        for server in self.config['servers']:
            client = self.IRCClient(
                nickname=self.config.get('nick', 'tg'),
                username=self.config.get('username', 'telegram'),
                realname=self.config.get('realname', 'Telegram relay')
            )
            client.bot = self
            client.config = server

            client.connect('irc.rizon.net', tls=False)

            for chat in server.get('chats', []):
                self.chats[chat] = self.chats.get('chat', set())
                self.chats[chat].add(client)

    async def on_chat_message(self, msg):
        content_type, chat_type, chat_id = telepot.glance(msg)

        if content_type == 'new_chat_member':
            logger.info('Added to telegram channel %s', msg['chat']['id'])

        for client in self.chats.get(chat_id, []):
            for channel in client.config.get('channels', []):
                client.message(channel, self.msg_to_energymech(msg))

    def msg_to_energymech(self, msg):
        # timestamp
        date = datetime.datetime.fromtimestamp(msg['date'])
        message = "[%s] " % date.strftime("%H:%M:%S")

        # prefix, with username and forward/reply marking
        prefix = "<%s>" % msg['from'].get(
            'username', msg['from'].get('id', -1))

        if 'forward_from' in msg:
            prefix += ' (Forward from @%s)' % msg['forward_from'].get(
                'username', msg['from'].get('id', -1))

        if 'reply_to_message' in msg:
            prefix += ' (Reply to @%s)' % msg['reply_to_message']['from'].get(
                'username', msg['from'].get('id', -1))

        # text
        text = self.sanitize(msg.get('text', ''))

        # space after the text
        if text:
            message += '%s %s' % (prefix, text)

        # check if it's a file type message
        file_type = self.contains(
            msg,
            ['audio', 'document', 'photo', 'video', 'voice', 'sticker'])

        if file_type:
            if (isinstance(msg[file_type], list)):
                f = self.largest_picture(msg[file_type])
            else:
                f = msg[file_type]

            message += '%s %s:https://european.shitposting.agency/tg/file/%s' % (
                prefix, file_type, f['file_id'])

            if 'file_name' in f:
                message += '/%s' % f['file_name']

        elif 'contact' in msg:
            message += '%s Contact: %s' % (
                prefix,
                self.get_name(msg['contact']))

        elif 'venue' in msg:
            message += '%s Venue: %s, %s - %s' % (
                prefix,
                msg['venue']['title'],
                msg['venue']['address'],
                self.location_to_coords(msg['venue']['location']))

        elif 'location' in msg:
            message += '%s Location: %s' % (
                prefix,
                self.location_to_coords(msg['location']))

        elif 'new_chat_member' in msg:
            message += '*** Joins: %s (%s)' % (
                msg['new_chat_member'].get('username', -1),
                self.get_hostname_equivalent(msg['new_chat_member'])
            )

        elif 'left_chat_member' in msg:
            message += '*** Parts: %s (%s)' % (
                msg['left_chat_member'].get('username', -1),
                self.get_hostname_equivalent(msg['left_chat_member'])
            )

        elif 'new_chat_title' in msg:
            message += "*** %s changes topic to '%s'" % (
                msg['from']['username'],
                msg['new_chat_title']
            )

        elif 'new_chat_photo' in msg:
            message += "*** %s changes photo to photo:%s" % (
                msg['from']['username'],
                self.largest_picture(msg['new_chat_photo'])['file_id']
            )

        elif 'delete_chat_photo' in msg:
            message += "*** %s deletes channel photo" % (
                msg['from']['username']
            )

        message += self.sanitize(msg.get('caption', ''))
        return message


__bot__ = IRCRelay
