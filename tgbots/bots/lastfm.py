import asyncio
import logging
import re
import urllib.parse

import lxml.etree
import pylast
import telepot
import telepot.async

import tgbots.util
from tgbots.util import run_async

logger = logging.getLogger(__name__)

"""
A last.fm bot for telegram
"""


class LastFm(tgbots.util.Bot):
    def __init__(self, config, persistence):
        super(LastFm, self).__init__(config, persistence)

        auth_config = self.config['auth']

        # get bot info
        self.me = asyncio.get_event_loop().run_until_complete(self.getMe())

        # load the last.fm API client
        self.lfm = pylast.LastFMNetwork(
            api_key=auth_config['key'],
            api_secret=auth_config['secret'])

        # create the name cache if it doesn't exist
        self.pers['names'] = self.pers.get('names', {})

    def sanitize(self, s):
        return re.sub('[_*]', '\\\0', s)

    def youtube(self, track):
        artist, title = track.get_artist(), track.get_name()

        query = urllib.parse.urlencode({
            'q': "site:youtube.com %s - %s" % (artist, title),
            'btnI': 'I'
        })

        url = "<a href='https://www.google.com/webhp?#%s'>%s</a>" % (
            query,
            title)

        return url

    async def playing_answer(self, user):
        # must request manually as the api removes tracks if they're nowplaying
        doc = await run_async(
            user._request,
            user.ws_prefix + '.getRecentTracks',
            False)

        tracks = doc.getElementsByTagName('track')

        if len(tracks) == 0:
                return None

        e = tracks[0]
        track = pylast.Track(
            pylast._extract(e, 'artist'),
            pylast._extract(e, 'name'),
            user.network,
            user.name)

        # get track info
        params = track._get_params().copy()
        params.update({
            'username': user.name
        })

        # get more track information
        track_info = await run_async(
            track._request,
            track.ws_prefix + ".getInfo",
            True,
            params)

        # use etree for xpath
        root = lxml.etree.XML(track_info.toxml())

        # get the medium image
        try:
            img = root.find('./track/album/image[@size="extralarge"]').text
            thumb = '<a href="%s">\u2063</a>' % img
        except AttributeError:
            img = None
            thumb = ""

        # add loved emoji
        track = self.youtube(track)
        if root.find('./track/userloved').text == '1':
            track += ' ❤️'

        # check if now playin or last played
        state = 'Now Playing' if e.hasAttribute('nowplaying') \
            else 'Last Played'

        # ordinalize play count
        play_count = int(root.find('./track/userplaycount').text)

        if play_count <= 1:
            play_count = 'first'
        else:
            play_count = tgbots.util.ordinal(play_count)

        # return edited message
        return {
            'text': '%s<b>%s for the %s time:</b>\n%s' % (
                thumb,
                state,
                play_count,
                track),
            'parse_mode': 'HTML'
        }

    async def top_artists_answer(self, user):
        tasks = [
            asyncio.ensure_future(
                run_async(user.get_top_artists, limit=10)),
            asyncio.ensure_future(
                run_async(user.get_top_albums, limit=1)),
            asyncio.ensure_future(
                run_async(user.get_top_tracks, limit=1)),

            asyncio.ensure_future(
                run_async(user.get_top_artists, limit=10, period='1month')),
            asyncio.ensure_future(
                run_async(user.get_top_albums, limit=1, period='1month')),
            asyncio.ensure_future(
                run_async(user.get_top_tracks, limit=1, period='1month')),
        ]
        answers = await asyncio.gather(*tasks)

        top_artists, top_albums, top_tracks, \
            top_artists_last, top_albums_last, top_tracks_last = answers

        text = ""
        if len(top_artists):
            text += "<b>Overall</b> I'm into %s.\n\n" % (
                ", ".join([artist.item.name for artist in top_artists]))

            try:
                text += "<b>Top Album</b>: %s (%s plays)\n" % (
                    top_albums[0].item, top_albums[0].weight)
                text += "<b>Top Track</b>: %s (%s plays)" % (
                    top_tracks[0].item, top_tracks[0].weight)
            except IndexError:
                pass

        if len(top_artists) and len(top_artists_last):
            text += '\n\n'

        if len(top_artists_last):
            text += "<b>Last month</b> I've been into %s\n\n" % (
                ", ".join([artist.item.name for artist in top_artists_last]))

            try:
                text += "<b>Top Album</b>: %s (%s plays)\n" % (
                    top_albums_last[0].item, top_albums_last[0].weight)
                text += "<b>Top Track</b>: %s (%s plays)" % (
                    top_tracks_last[0].item, top_tracks_last[0].weight)
            except IndexError:
                pass

        return {
            'text': text,
            'parse_mode': 'HTML'
        }

    async def on_chat_message(self, msg):
        content_type, chat_type, chat_id = telepot.glance(msg)

        if chat_type == 'private':
            await self.sendMessage(
                chat_id,
                parse_mode='Markdown',
                text='This is an inline bot. To use it, type\n' +
                '`@%s [your last.fm username]`\n' % self.me['username'] +
                'and wait a few moments.')

    async def on_inline_query(self, msg):
        query_id, from_id, query_string = telepot.glance(
            msg, flavor='inline_query')

        query_string = query_string.strip()

        if len(query_string) == 0:
            query_string = self.pers['names'].get(str(from_id))
            if not query_string:
                return

        loading_message = 'Fetching data...'

        reply_markup = {
            'inline_keyboard': [
                [{
                    'text': 'Loading...',
                    'callback_data': 'loading'
                }]
            ]
        }

        await self.answerInlineQuery(
            query_id, [
                {
                    'id': 'playing:%s' % query_string,
                    'type': 'article',
                    'title': 'Now Playing for %s' % query_string,
                    'description': "Send what you're listening to.",
                    'message_text': loading_message,
                    'reply_markup': reply_markup
                },
                {
                    'id': 'summary:%s' % query_string,
                    'type': 'article',
                    'title': 'Summary for %s' % query_string,
                    'description': "Send your taste.",
                    'message_text': loading_message,
                    'reply_markup': reply_markup
                }],
            cache_time=1
        )

    # need `/setinlinefeedback`
    async def on_chosen_inline_result(self, msg):
        result_id, from_id, query_string = telepot.glance(
            msg, flavor='chosen_inline_result')

        # get query type and query string from result id
        query_type, query_string = result_id.split(':', 1)

        # save query string
        self.pers['names'][str(from_id)] = query_string

        user = await run_async(self.lfm.get_user, query_string)

        try:

            if query_type == 'playing':
                result = await self.playing_answer(user)

            elif query_type == 'summary':
                result = await self.top_artists_answer(user)

            if result is None:
                await self.editMessageText(
                    msg['inline_message_id'],
                    text='An error ocurred.')
                return

            await self.editMessageText(
                msg['inline_message_id'],
                **result)

        except pylast.WSError as e:
            if e.details == 'User not found':
                await self.editMessageText(
                    msg['inline_message_id'],
                    text='User not found.')
                return

            else:
                await self.editMessageText(
                    msg['inline_message_id'],
                    text='An error ocurred: %s.' % e.details)
                return
__bot__ = LastFm
