import collections
import logging
import re
import random
import time

import telepot
import telepot.async

import asyncio
import tgbots.util
from . import constituicao
import os

logger = logging.getLogger(__name__)


class LeiPortuguesa(tgbots.util.Bot):
    """
    Portuguese law consultation bot
    """
    def __init__(self, config, persistance):
        super(LeiPortuguesa, self).__init__(config, persistance)

        script_dir = os.path.dirname(os.path.realpath(__file__))
        with open(os.path.join(script_dir, 'constituicao.txt')) as f:
            text = f.read()

        self.constituicao = constituicao.parse_txt(text)

        # mapping of lowercase types and the classes
        self.const_search_types = {
            t.__name__.lower(): t
            for t in self.constituicao.child_types()}

        # usage
        self.usage = ''

        ks = ', '.join(self.const_search_types.keys())
        self.usage += "/constituicao <tipo> <num|titulo|aleat>\n"
        self.usage += "tipo: %s\n" % ks

        self.usage += "/constituicao procurar <texto|titulo>\n"

        # get bot info
        self.me = asyncio.get_event_loop().run_until_complete(self.getMe())

    def const_obj_title(self, obj):
        output = ''
        if obj.number:
            output += '<b>%s %s</b>' % (
                obj.__class__.__name__,
                obj.number)

            if obj.title:
                output += ' - %s' % obj.title

        else:
            if obj.title:
                output += '<b>%s</b>' % obj.title
        return output

    def const_obj_cmd(self, obj):
        output = '/constituicao %s ' % obj.__class__.__name__.lower()
        if obj.number:
            output += str(int(obj.number))
            return output
        else:
            if obj.title:
                output += obj.title
                return output

    async def send_constitution_result(self, result, msg):
        """
        Sends a constitution result as a reply to a message
        """

        output = ''

        parents = result.parents(True)
        for parent in parents[1:]:
            output += '\n%s' % self.const_obj_title(parent)

        output += '\n\n%s' % result.text

        if result.text == '':
            for child in result.children:
                output += '%s\n' % self.const_obj_title(child)

                cmd = self.const_obj_cmd(child)
                #output += '<code>%s</code>\n\n' % cmd

        return await self.sendMessage(
            msg['chat']['id'],
            reply_to_message_id=msg['message_id'],
            text=output,
            parse_mode='HTML')

    async def on_chat_message(self, msg):
        def check(a, b, minlen=None):
            """
            returns a[b] if it exists.
            otherwise returns None.
            if minlen is set returns None if len(a) < minlen
            """
            if minlen and len(a) < minlen:
                return None

            try:
                return a[b]
            except IndexError:
                return None

        if 'text' in msg:
            text = msg['text']

            if text.startswith('/'):
                args = text.split(' ')

                # check and remove mention from command
                if '@' in args[0]:
                    if args[0].split('@')[1].lower() == self.me['username'].lower():
                        args[0] = args[0].split('@')[0]

                if args[0].lower() in [
                        '/help',
                        '/ajuda',
                        '/start']:
                    await self.sendMessage(
                        msg['chat']['id'],
                        reply_to_message_id=msg['message_id'],
                        text=self.usage,
                        parse_mode='Markdown')
                    return

                # /constituição
                # /constituicao
                # /constitution
                if args[0].lower() in [
                        '/constituicao',
                        '/constituição',
                        '/constitution']:

                    # search <text>
                    if check(args, 1) == 'procurar':
                        if len(args) == 1:
                            return

                        # search in title
                        search = self.constituicao.findall(
                            title=constituicao.contains(' '.join(args[2:]))
                        )

                        # search in body
                        search += self.constituicao.findall(
                            text=constituicao.contains(' '.join(args[2:]))
                        )

                        # remove duplicates without breaking order
                        search_dict = collections.OrderedDict()
                        for item in search:
                            search_dict[item] = None
                        search = list(search_dict.keys())

                        if len(search) == 0:
                            await self.sendMessage(
                                msg['chat']['id'],
                                reply_to_message_id=msg['message_id'],
                                text='Não foi encontrado nenhum resultado.',
                                parse_mode='HTML')
                            return

                        output = ''
                        if len(search) == 1:
                            await self.send_constitution_result(search[0], msg)
                            return

                        else:
                            # max 5 results
                            limit = 5
                            for result in search[0:limit]:
                                title = self.const_obj_title(result)
                                cmd = self.const_obj_cmd(result)
                                output += '%s\n' % title

                                # hide unselectable results
                                if cmd is None:
                                    continue

                                output += '<code>%s</code>\n\n' % cmd

                            if len(search) > limit:
                                output += 'mais <b>%i</b> resultados.' % (
                                    len(search) - limit
                                )

                            await self.sendMessage(
                                msg['chat']['id'],
                                reply_to_message_id=msg['message_id'],
                                text=output,
                                parse_mode='HTML')
                            return

                    # artigo|titulo|etc... <num|title|aleat>
                    arg1 = check(args, 1, minlen=2)
                    if arg1 in self.const_search_types.keys():
                        search = ' '.join(args[2:])

                        # random result
                        if len(args) == 3 and search.startswith('aleat'):
                            result = random.choice(self.constituicao.findall(
                                self.const_search_types[arg1]))

                        else:
                            try:
                                # search by number
                                result = self.constituicao.find(
                                    self.const_search_types[arg1],
                                    number=int(search)
                                )
                            except ValueError:
                                # search by title
                                result = self.constituicao.find(
                                    self.const_search_types[arg1],
                                    title=constituicao.contains(search)
                                )

                        if result:
                            await self.send_constitution_result(result, msg)
                            return

                        else:
                            await self.sendMessage(
                                msg['chat']['id'],
                                reply_to_message_id=msg['message_id'],
                                text='%s %s não encontrado' % (
                                    self.const_search_types[arg1].__name__,
                                    args[2]
                                ),
                                parse_mode='HTML')
                            return

                        return

__bot__ = LeiPortuguesa
