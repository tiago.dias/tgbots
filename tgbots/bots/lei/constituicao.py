import logging
import numbers
import re

import roman
import inspect

log = logging.getLogger(__name__)


class contains(object):
    """
    pass as a keyword argument to find() and findall() to search for text
    containing `value`.
    """
    def __init__(self, value):
        self.value = value


class Objecto(object):
    child_type = None
    number_style = None
    second_line_title = True

    class Number(object):
        def __init__(self, number, number_type='normal'):
            self.number_type = number_type

            if self.number_type == 'roman':
                self._number = roman.fromRoman(number)
            else:
                self._number = int(number)

            pass

        def __int__(self):
            return self._number

        def __repr__(self):
            if self.number_type == 'roman':
                return roman.toRoman(self._number)
            elif self.number_type == 'ordinal':
                return '%i.º' % self
            else:
                return str(self._number)

        def __eq__(self, other):
            if isinstance(other, int):
                return (other == self._number)

            elif isinstance(other, self.__class__):
                return (int(other) == self._number)

            elif isinstance(other, str):
                return(other == str(self._number))

            else:
                return super().__eq__(other)

        def __ne__(self, other):
            return not self.__eq__(other)

        def __str__(self):
            return self.__repr__()

    def __init__(self, title=None, number=None, text='', parent=None):
        self.title = title
        self.number = number
        self.text = text
        self.parent = parent
        self.children = []
        self.prefix = ''

    @property
    def number(self):
        return self._number

    @number.setter
    def number(self, value):
        try:
            self._number = self.Number(value, number_type=self.number_style)
        except TypeError:
            self._number = value

    @number.deleter
    def number(self):
        del self._number

    def find_parent_of(self, clazz):
        """
        finds a parent up in the tree with the specified type as a child type
        """
        # Artigo can be a child of anything, except itself!
        if self.__class__ is not Artigo and clazz is Artigo:
            return self

        if self.child_type is clazz:
            return self

        if self.parent:
            if self.parent.child_type is clazz:
                return self.parent

            else:
                return self.parent.find_parent_of(clazz)

        return None

    @classmethod
    def child_types(self, first=True):
        """
        returns a list with all the children types, recursive
        """
        if first:
            output = [Artigo]
        else:
            output = []

        if self.child_type is not None:
            output += [self.child_type]
            output += self.child_type.child_types(first=False)
        else:
            output += []

        return output

    def print_tree(self, level=0):
        """
        prints a tree view
        """

        def iprint(s):
            """
            prints a string with the current recursion indentation
            """

            print('%s%s' % (
                '   ' * level,
                s
            ))

        descriptor = self.__class__.__name__
        if self.number:
            descriptor += ' %s' % self.number
        if self.title:
            descriptor += ' - %s' % self.title

        iprint(descriptor)

        for child in self.children:
            child.print_tree(level+1)

    def findall(self, typ=None, single=False, **kwargs):
        """
        finds every child with the specified parameters
        """
        finds = []
        try:
            for parameter, value in kwargs.items():
                check_contains = False
                if value.__class__ is contains:
                    check_contains = True
                    value = value.value

                if check_contains:
                    if value not in getattr(self, parameter):
                        raise AttributeError
                else:
                    if getattr(self, parameter) != value:
                        raise AttributeError

            if (typ is not None) and (self.__class__ is not typ):
                raise AttributeError

            if single:
                return self
            else:
                finds.append(self)

        except AttributeError:
            pass

        for child in self.children:
            child_finds = child.findall(typ, single=single, **kwargs)
            if single:
                if child_finds is not None:
                    return child_finds

            else:
                finds += child_finds

        if single:
            return None
        else:
            return finds

    def find(self, typ=None, **kwargs):
        """
        finds a child with the specified parameters
        """
        return self.findall(typ, single=True, **kwargs)

    def shorttitle(self):
        """
        returns the title for this object, in the form of a dictionary
        """
        output = {}
        title_str = ''

        if self.number:
            output.update({
                'class': self.__class__.__name__,
                'number': self.number
            })

            title_str += '%s %s' % (output['class'], output['number'])

            if self.title:
                title_str += ' - '

        if self.title:
            output.update(title=self.title)
            title_str += self.title

        output['str'] = title_str
        return output

    def fulltitle(self):
        """
        returns a the title of itself and all the parents as a list of
        dictionaries
        """
        parts = []
        for parent in self.parents():
            parts.append(parent.shorttitle())
        parts += [self.shorttitle()]

        return parts

    def parents(self, include_itself=False):
        """
        returns a list of all the parents
        """
        if include_itself:
            parents = [self]
        else:
            parents = []

        if self.parent:
            parents = self.parent.parents(include_itself=True) + parents

        return parents

    def __str__(self):
        return self.shorttitle()['str']

    def __repr__(self):
        return self.shorttitle()['str']


class Artigo(Objecto):
    prefix = 'Artigo'
    number_style = 'ordinal'
    pass


class Capitulo(Objecto):
    prefix = 'CAPÍTULO'
    number_style = 'roman'
    pass


class Titulo(Objecto):
    child_type = Capitulo
    prefix = 'TÍTULO'
    number_style = 'roman'
    capitulos = []


class Parte(Objecto):
    child_type = Titulo
    prefix = 'PARTE'
    number_style = 'roman'
    titulos = []


class Livro(Objecto):
    child_type = Parte
    second_line_title = False

    # need to set prefix like this because of loss of formatting.
    prefix = (r'' +
              r'(?P<title>PREÂMBULO|' +
              r'Princípios fundamentais|' +
              r'Disposições finais e transitórias)')


class Documento(Objecto):
    child_type = Livro


def parse_txt(text):
    """
    Processes a copy-paste of raw text from
    http://www.parlamento.pt/Legislacao/Paginas/ConstituicaoRepublicaPortuguesa.aspx
    (NO HTML)
    """
    chapters = []

    # documento
    #   livro
    #       parte
    #           título
    #               capítulo
    #
    # artigo can be inside either

    num_regex = {
        'ordinal': r'(?P<number>\d+).º',
        'roman': r'(?P<number>[IVXLCDM]+)'
    }

    # the root "document" object
    root = Documento()
    curr_obj = root

    # objects type that can be matched on each loop.
    # artigo can be a child of any type, so it isn't ever defined as
    # a `child_type`.
    can_match = curr_obj.child_types()

    # sort of state machine. state is represented by current object
    # and its properties.
    for line in text.split('\n'):
        # remove trailing and leading whitespace
        line = line.strip()

        log.debug('-')
        log.debug('Line: %s', line)
        log.debug('Inside: %s', curr_obj.__class__.__name__)
        log.debug('Current Title: %s', curr_obj.title)
        log.debug('Number: %s', curr_obj.number)

        if line != '':
            # find title on the first/second line for objects that have it
            if curr_obj.second_line_title or curr_obj.number_style is None:
                if curr_obj.title is None:
                    curr_obj.title = line

                    # title set
                    log.debug('Set current object title: %s' % line)
                    continue

            new_obj = False
            for Class in can_match:
                # match the child type
                prefix = Class.prefix
                if Class.number_style is not None:
                    regex = num_regex[Class.number_style]
                else:
                    regex = ''
                pattern = (prefix + ' ' + regex).strip()

                match = re.match(pattern, line)
                if match:
                    # matching with class
                    log.debug('Matched: %s', Class.__name__)

                    # find the parent
                    parent = curr_obj.find_parent_of(Class)

                    # convert the number type, if existing, else get name.
                    number = None
                    title = None
                    if Class.number_style is None:
                        title = match.group('title')

                        # title set
                        log.debug('New object title: %s' % line)

                    else:
                        number = match.group('number')

                    # number processed
                    log.debug('Number: %s', number)

                    # create a new object
                    obj = Class(
                        title=title,
                        number=number,
                        parent=parent)

                    # clean the text of the finished element
                    curr_obj.text = curr_obj.text.strip()

                    # add the object to the parrent, and switch context
                    parent.children.append(obj)
                    curr_obj = obj

                    # match found
                    log.debug('Pattern: %s', pattern)
                    log.debug('Match: %s', Class.__name__)

                    descriptor = obj.__class__.__name__
                    if obj.title:
                        descriptor += ' %s' % obj.title

                    if obj.number:
                        descriptor += ' %s' % obj.number

                    # a new object was found...
                    new_obj = True
                    break

            if new_obj:
                # ...on to the next line
                continue

        # title has been processed. append to the text
        if curr_obj.title is not None:
            curr_obj.text += line + '\n'

            # text appended
            log.debug('Text appended.')
            continue

        # line not processed
        log.critical('Line not processed: %s' % line)
        return

    # we're done!
    return root
