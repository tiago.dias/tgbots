import os
import time
import json
import sys
import mimetypes
import uuid
import logging

import telepot
import telepot.async

import asyncio
import tgbots.util

import aiohttp
from aiohttp import web

logger = logging.getLogger(__name__)


class Log(tgbots.util.Bot):
    """
    A telegram log bot
    """
    GOOGLE_API = 'https://www.googleapis.com/urlshortener/v1/url?key=%s'

    def __init__(self, config, persistance):
        super(Log, self).__init__(config, persistance)

        output_dir = self.config.get('output_dir', './log/channels')
        os.makedirs(output_dir, exist_ok=True)

        # get bot info
        self.me = asyncio.get_event_loop().run_until_complete(self.getMe())

        # persistence default values
        self.pers['shortening'] = self.pers.get('shortening', {})

        # start the http server
        if config.get('server', False):
            app = web.Application()
            app.router.add_route('GET', '/', self.index)
            app.router.add_route('GET', '/{id}/{name}', self.on_http)
            app.router.add_route('GET', '/{id}', self.on_http)

            loop = asyncio.get_event_loop()
            server = loop.create_server(
                app.make_handler(),
                '0.0.0.0',
                self.config.get('port', 8080))
            loop.run_until_complete(server)

    def contains(self, a, b):
        """Returns the element if any of the elements in a is in b"""
        for element in a:
            if element in b:
                return element
        return None

    def largest_picture(self, pictures):
        return max(pictures, key=lambda photo: photo['file_size'])

    async def shorten(self, url, google_key):
        async with aiohttp.post(
                url= self.GOOGLE_API % google_key,
                headers={
                    'content-type': 'application/json'
                },
                data=json.dumps({
                    'longUrl': url})
                ) as r:

            data = await r.json()

            if r.status != 200:
                raise Exception(data.get('error', {}).get('message'))

            else:
                return data.get('id', url)

    async def send_usage(self, chat_id):
        await self.sendMessage(
            chat_id,
            'There are several ways to use this bot:\n\n' +
            '<b>1.</b> send the /url command as a reply to some file you want a URL of;\n' +
            '<b>2.</b> send the /url command as the caption of some file you want a URL of;\n' +
            '<b>3.</b> send the bot a file you want a URL of.\n\n' +
            'Files can be anything you can upload to telegram, like images, audio, or videos.',
            parse_mode='HTML')

    async def index(self, request):
        return web.Response(text="https://telegram.me/%s" % self.me['username'])

    async def send_url(self, msg):
        content_type, chat_type, chat_id = telepot.glance(msg)
        thing = msg.get('reply_to_message', msg)

        file_type = self.contains(
            msg,
            ['audio', 'document', 'photo', 'video', 'voice',
             'sticker'])

        if file_type:
            if (isinstance(msg[file_type], list)):
                f = self.largest_picture(msg[file_type])
            else:
                f = msg[file_type]

            url = self.gateway_url(file_type, f)

            logger.info('Sending %s to %s',
                        url,
                        tgbots.util.username(msg['from']))

            if self.config.get('shorten'):
                url = await self.shorten(
                    url,
                    google_key=self.config.get('shorten'))

            await self.sendMessage(
                chat_id,
                url,
                reply_to_message_id=msg['message_id'],
                disable_web_page_preview=True)
            return

        else:
            await self.send_usage(chat_id)
            return

    def gateway_url(self, file_type, f):
        # few thousand years to crack
        shorten_id = str(uuid.uuid4()).replace('-', '')[0:14]
        self.pers['shortening'][shorten_id] = f['file_id']

        return "%s/%s" % (
            self.config.get('http', ''),
            shorten_id
        )

    async def on_chat_message(self, m):
        msg = m.get('reply_to_message', m)
        content_type, chat_type, chat_id = telepot.glance(msg)

        if self.config.get('log', True):
            output_dir = self.config.get('output_dir', './log/channels')
            output_file = os.path.join(output_dir, str(chat_id) + '.log')

            with open(output_file, "a") as f:
                f.write(json.dumps(msg) + '\n')

        if self.config.get('server', False):
            if chat_type == 'private':
                await self.send_url(msg)
                return

            text = m.get('text', m.get('caption', ''))

            if text.startswith('/'):
                args = text.split(' ')

                # check if we have a mention on the command
                if '@' in args[0]:
                    if args[0].split('@')[1] == self.me['username']:
                        args[0] = args[0].split('@')[0]

                # /url
                if args[0] == '/url':
                    await self.send_url(msg)


    async def on_http(self, request):
        # look for short id
        shortened_id = request.match_info.get('id')
        file_id = self.pers['shortening'].get(shortened_id)

        # no short id, use long id
        file_id = request.match_info.get('id')

        # get the file info
        try:
            f = await self.getFile(file_id)
        except telepot.exception.TelegramError as e:
            return web.Response(text=e.description, status=e.error_code)
        server_filename, extension = os.path.splitext(f['file_path'])

        # download the file
        res = await aiohttp.get("https://api.telegram.org/file/bot%s/%s" % (
            self.config['key'], f['file_path']
        ))

        body = await res.read()

        # find mime type
        mimetype = None
        extension = f['file_path'].split('.')[-1]

        if extension == 'webp':
            mimetype = 'image/webp'

        file_name = request.match_info.get('name', None)
        if not file_name:
            file_name = file_id + '.%s' % extension

        mimetype = mimetypes.types_map.get('.' + extension, mimetype)

        headers = aiohttp.CIMultiDict(res.headers)
        if mimetype:
            headers.add(
                'Content-Type',
                mimetype)

            headers.add(
                'Content-Disposition',
                'inline; filename="%s"' % file_name
            )

        return web.Response(body=body, headers=headers)

__bot__ = Log
