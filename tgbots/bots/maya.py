import re

import time
import aiohttp
import telepot
import telepot.async
from bs4 import BeautifulSoup
from fuzzywuzzy import process

import asyncio
import tgbots.util
from tgbots.util import run_async

"""
An astrology bot
"""

# table relating sign UTF-8 names with their ascii and emoji counterparts
signs = {
  "Carneiro": {
    "ascii": "carneiro",
    "emoji": "\u2648"
  },
  "Touro": {
    "ascii": "touro",
    "emoji": "\u2649"
  },
  "Gémeos": {
    "ascii": "gemeos",
    "emoji": "\u264A"
  },
  "Carangueijo": {
    "ascii": "cancer",
    "emoji": "\u264B"
  },
  "Leão": {
    "ascii": "leao",
    "emoji": "\u264C"
  },
  "Virgem": {
    "ascii": "virgem",
    "emoji": "\u264D"
  },
  "Balança": {
    "ascii": "balanca",
    "emoji": "\u264E"
  },
  "Escorpião": {
    "ascii": "escorpiao",
    "emoji": "\u264F"
  },
  "Sagitário": {
    "ascii": "sagitario",
    "emoji": "\u2650"
  },
  "Capricórnio": {
    "ascii": "capricornio",
    "emoji": "\u2651"
  },
  "Aquário": {
    "ascii": "aquario",
    "emoji": "\u2652"
  },
  "Peixes": {
    "ascii": "peixes",
    "emoji": "\u2653"
  }
}


class Maya(tgbots.util.Bot):
    def __init__(self, config, persistance):
        super(Maya, self).__init__(config, persistance)

        self.cache = {}

        # get bot info
        self.me = asyncio.get_event_loop().run_until_complete(self.getMe())

    async def answer(self, sign, astrologist='maya', forecast='diaria'):
        url = 'http://lifestyle.sapo.pt/astral/previsoes/%s?signo=%s' % (
            astrologist,
            signs[sign]['ascii'])

        r = await aiohttp.get(url)

        if r.status != 200:
            return None

        # parse the content
        text = await r.text()
        soup = await run_async(BeautifulSoup, text, 'html.parser')

        try:
            output = "%s *%s, " % (signs[sign]['emoji'], sign)
            output += soup.find(id=forecast).find('h4').text.strip() + "*\n"

            for t in soup.find(id=forecast).findAll('p'):
                output += "\n%s " % re.sub(
                    r"(.+?)(:| -)\D",
                    lambda m: "*%s*: " % m.group(1).title(), t.text)
        except AttributeError:
            return None

        output += "\n\n~ %s" % astrologist.title()

        if forecast == 'diaria':
            forecast_text = 'Diária'
        else:
            forecast_text = forecast.title()

        return {
            'id': '%s-%s' % (forecast, sign),
            'type': 'article',
            'title': 'Previsão %s - %s' % (forecast_text, sign),
            'message_text': output,
            'parse_mode': 'Markdown'
        }

    async def on_chat_message(self, msg):
        content_type, chat_type, chat_id = telepot.glance(msg)

        if chat_type == 'private':
            await self.sendMessage(
                chat_id,
                parse_mode='Markdown',
                text='This is an inline bot. To use it, type\n' +
                '`@%s [your sign]`\n' % self.me['username'] +
                'and wait a few moments.')

    async def on_inline_query(self, msg):
        query_id, from_id, query_string = telepot.glance(
            msg, flavor='inline_query')

        sign = process.extractOne(query_string.lower(), signs.keys())[0]

        if not sign:
            return

        # current UNIX timestamp
        timestamp = time.time()

        # get the data and cache, if the cache is too old
        max_age = self.config.get('max_age', 600)
        if (sign not in self.cache) \
                or (timestamp - self.cache[sign]['timestamp'] > max_age):

            # get all the forecats concurrently
            tasks = [
                asyncio.ensure_future(self.answer(sign, forecast='diaria')),
                asyncio.ensure_future(self.answer(sign, forecast='semanal'))
            ]

            # extract the results
            answers = await asyncio.gather(*tasks)

            self.cache[sign] = {
                'timestamp': timestamp,
                'answers': answers
            }

        answers = self.cache[sign]['answers']

        await self.answerInlineQuery(
            query_id,
            [answer for answer in answers if answer is not None],
            cache_time=1)

        return []

    def on_chosen_inline_result(self, msg):
        pass

__bot__ = Maya
