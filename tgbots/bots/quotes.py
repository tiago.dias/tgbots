import telepot
import logging
import telepot.async
import random

import asyncio
import json
import tgbots.util

import datetime

logger = logging.getLogger(__name__)

class Quotes(tgbots.util.Bot):
    """
    Quote bot
    """
    def __init__(self, config, persistance):
        super(Quotes, self).__init__(config, persistance)

        # load quotes
        self.last_quote = {}
        for channel, metadata in self.pers['quotes'].items():
            try:
                self.last_quote[str(channel)] = int(max(
                    metadata['quotes'],
                    key=lambda quote: int(quote)))
            except ValueError:
                self.last_quote[str(channel)] = 0

        # get bot info
        self.me = asyncio.get_event_loop().run_until_complete(self.getMe())

    async def send_quote(self, channel, quoteno):
        try:
            quote = self.pers['quotes'][str(channel)]['quotes'][str(quoteno)]
        except KeyError:
            quote = None

        if quote:
            date = datetime.datetime.fromtimestamp(quote['date'])
            fmt_date = date.strftime("%b %d, %Y")

            text = 'Quote #%s from %s,\nAdded by %s:\n\n%s' % (
                quoteno,
                fmt_date,
                quote['author'],
                quote['text'])

            await self.sendMessage(
                channel,
                text,
                reply_to_message_id=quote.get('message_id', None))
        else:
            await self.sendMessage(
                channel,
                'This quote was not found.')

    async def on_chat_message(self, msg):
        content_type, chat_type, chat_id = telepot.glance(msg)
        message_id = msg['message_id']

        if chat_type == 'private':
            await self.sendMessage(
                chat_id, 'I am a group bot! Add me to a group to try me out!')
            return

        if chat_type == 'group':
            if content_type == 'text':
                text = msg['text']
                if text.startswith('/'):
                    args = text.split(' ')

                    # check if we have a mention on the command
                    if '@' in args[0]:
                        if args[0].split('@')[1] == self.me['username']:
                            args[0] = args[0].split('@')[0]

                    # /addquote
                    if args[0] == '/addquote':
                        if 'reply_to_message' not in msg:
                            print(msg)
                            await self.sendMessage(
                                chat_id,
                                'Send this command as a reply to the message you want to quote')
                            return

                        if str(chat_id) not in self.pers['quotes']:
                            self.pers['quotes'][str(chat_id)] = {'quotes': {}}

                        quoted_msg = msg['reply_to_message']
                        quoted_user = msg['reply_to_message']['from']

                        if 'forward_from' in msg['reply_to_message']:
                            quoted_user = msg['reply_to_message']['forward_from']
                            date = msg['reply_to_message']['forward_date']
                        else:
                            date = msg['reply_to_message']['date']

                        if 'username' in quoted_user:
                            quoted_username = quoted_user['username']
                        elif 'first_name' in quoted_user:
                            quoted_username = quoted_user['first_name']
                            if 'last_name' in quoted_user:
                                quoted_username += " "
                                quoted_username += quoted_user['last_name']

                        text = '<%s> ' % quoted_username
                        text += msg['reply_to_message'].get('text', '')
                        text += msg['reply_to_message'].get('caption', '')

                        if 'username' in msg['from']:
                            author = '@' + msg['from']['username']
                        else:
                            author = 'Unknown'

                        try:
                            self.last_quote[str(chat_id)] += 1
                        except KeyError:
                            self.last_quote[str(chat_id)] = 1

                        quote_id = str(self.last_quote[str(chat_id)])

                        self.pers['quotes'][str(chat_id)]['quotes'][quote_id] = {
                            "author": author,
                            "date": date,
                            "text": text,
                            "message_id": quoted_msg['message_id']
                        }

                        await self.sendMessage(
                            chat_id,
                            'Quote #%s added.' % quote_id,
                            reply_to_message_id=quoted_msg['message_id'])

                    # /searchquote
                    elif args[0] == '/searchquote':
                        if str(chat_id) not in self.pers['quotes']:
                            await self.sendMessage(
                                chat_id,
                                'This channel has no quotes.')
                            return

                        if len(args) <= 1:
                            await self.sendMessage(
                                chat_id,
                                'You must give me something to search for.')
                            return

                        matches = []
                        for quote, data in self.pers['quotes'][str(chat_id)]['quotes'].items():
                            if ' '.join(args[1:]).lower() in data['text'].lower():
                                matches.append('%s' % quote)

                        if matches:
                            output = 'Results: %s' % ', '.join(
                                ['#'+m for m in matches[0:10]])

                            if len(matches) == 1:
                                await self.send_quote(chat_id, matches[0])
                                return

                            elif len(matches) > 10:
                                output += ' and %i others' % (len(matches)-10)
                        else:
                            output = 'No results found.'

                        await self.sendMessage(
                            chat_id,
                            output)
                        return

                    # /quote
                    elif args[0] == '/quote':
                        if len(args) <= 1:
                            await self.sendMessage(
                                chat_id,
                                'You must give me a quote number.')
                            return

                        # random choice
                        if args[1] == 'random':
                            quote = random.choice(
                                list(
                                    self.pers['quotes'][str(chat_id)]['quotes'].keys()))

                        # last quote
                        elif args[1] == 'last':
                            quote = str(self.last_quote[str(chat_id)])

                        else:
                            quote = args[1]

                        await self.send_quote(chat_id, quote)

                    # /delquote
                    elif args[0] == '/delquote':
                        if msg['from'].get('username', '') not in self.config['admins']:
                            await self.sendMessage(
                                chat_id,
                                "You're not an administrator.")
                            return

                        if len(args) <= 1:
                            await self.sendMessage(
                                chat_id,
                                'You must give me a quote number.')
                            return

                        if self.pers['quotes'][str(chat_id)]['quotes'].pop(args[1], None):
                            await self.sendMessage(
                                chat_id,
                                'Quote #%s deleted.' % args[1])

                        else:
                            await self.sendMessage(
                                chat_id,
                                'That quote number does not exist.')
                            return

                    # /quotestats
                    elif args[0] == '/quotestats':
                        authors = {}
                        for quote, data in self.pers['quotes'][str(chat_id)]['quotes'].items():
                            authors[data['author']] = authors.get(data['author'], 0)+1

                        authors = [(author, quotes) for author, quotes in authors.items()]
                        authors = sorted(authors, key=lambda author: author[1], reverse=True)

                        text = '*Top 10 quote adders:*\n'
                        text += '\n'.join(['%s (%s)' % author for author in authors][0:10])

                        await self.sendMessage(
                            chat_id,
                            text,
                            parse_mode='Markdown')
                        return

__bot__ = Quotes
