import aiohttp
from aiohttp import web

import telepot
import telepot.async

import asyncio
import json
import tgbots.util

import magic


class Server(tgbots.util.Bot):
    """
    Image gateway
    """
    def __init__(self, config, persistance):
        super(Server, self).__init__(config, persistance)

        # start the http server
        app = web.Application()
        app.router.add_route('GET', '/file/{id}/{name}', self.on_http)
        app.router.add_route('GET', '/file/{id}', self.on_http)

        loop = asyncio.get_event_loop()
        server = loop.create_server(
            app.make_handler(),
            '0.0.0.0',
            self.config.get('port', 8080))
        loop.run_until_complete(server)

    async def on_http(self, request):
        file_id = request.match_info.get('id')
        file_name = request.match_info.get('name', None)

        # get the file info
        f = await self.getFile(file_id)

        # download the file
        res = await aiohttp.get("https://api.telegram.org/file/bot%s/%s" % (
            self.config['key'], f['file_path']
        ))

        body = await res.read()

        headers = aiohttp.CIMultiDict(res.headers)
        headers.add(
            'Content-Type',
            magic.from_buffer(body, mime=True).decode("utf-8"))

        return web.Response(body=body, headers=headers)

    async def on_chat_message(self, msg):
        content_type, chat_type, chat_id = telepot.glance(msg)

__bot__ = Server
