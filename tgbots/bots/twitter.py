import logging
import urllib.request
import html
import re

import tweepy

import asyncio
import tgbots.util
from tgbots.util import run_async

logger = logging.getLogger(__name__)


class Twitter(tgbots.util.Bot):
    """
    A twitter relay bot.

    Fetches tweets from a list of twitter accounts and shares them on
    predefined telegram channels.
    """

    def __init__(self, config, persistence):
        super(Twitter, self).__init__(config, persistence)

        # disable tweepy logging
        logging.getLogger('tweepy.binder').setLevel('WARNING')
        logging.getLogger('requests.packages.urllib3.connectionpool')\
            .setLevel('WARNING')

        self.pers['last_tweets'] = self.pers.get('last_tweets', {})
        auth_config = self.config['auth']

        auth = tweepy.OAuthHandler(
            auth_config['consumer_key'],
            auth_config['consumer_secret'])

        auth.set_access_token(
            auth_config['token'],
            auth_config['token_secret'])

        self.api = tweepy.API(auth)

        # start fetch loop for each following
        for channel, config in self.config['channels'].items():
            for i, user in enumerate(config['accounts']):
                # merge in the per-account config, if it exists
                if config.get('account_cfg', {}).get(user, None):
                    print(config)
                    config.update(config['account_cfg'][user])
                    print(config)

                asyncio.get_event_loop().create_task(self.loop(
                    channel, user, config, i
                ))

    async def loop(self, channel, user, config, delay_start):
        await asyncio.sleep(delay_start)
        while True:
            try:
                try:
                    await self.fetch(channel, user, config)
                except Exception as e:
                    logging.error(
                        'Exception occurred while fetching from %s',
                        user, exc_info=e)
            finally:
                await asyncio.sleep(self.config.get('interval', 60))

    async def fetch(self, channel, user, config):
        # this id is used to uniquely identify this channel-user pair
        subscription_id = "%s.%s" % (channel, user)

        logger.info("Fetching tweets from %s...", user)

        last_tweet = self.pers['last_tweets'].get(subscription_id, None)

        new_tweets = await run_async(
            self.api.user_timeline,
            screen_name=user,
            since_id=last_tweet,
            include_rts=config.get('include_retweets', False),
            exclude_replies=self.config.get('exclude_replies', True))

        new_tweets.reverse()  # reverse the order

        if len(new_tweets) > 0:
            logger.info("%i new tweets from %s.", len(new_tweets), user)

        # on first run only send the latest tweet
        if not last_tweet:
            new_tweets = new_tweets[-1:]

        for tweet in new_tweets:
            text = html.unescape(tweet.text)

            for fr, to in config.get('replace', {}).items():
                text = re.sub(fr, to, text)

            if config.get('prefix'):
                text = config['prefix'] + text

            if tweet.retweeted:
                if config.get('retweet_prefix', None):
                    text = ('%s ' % config.get('retweet_prefix', None)) + text

            tweet_url = 'https://twitter.com/%s/status/%s' % (
                tweet.user.screen_name,
                tweet.id
            )

            # check if there are no links in the tweet
            no_links = len(tweet.entities.get('urls', [])) == 0

            if (not config.get('no_src', False)) or \
                    (config.get('src_if_no_links', True) and no_links):

                text += '\n\n%s' % tweet_url

            try:
                extended_entities = tweet.extended_entities.items()
            except AttributeError:
                await self.sendMessage(
                    chat_id=channel,
                    text=text)
            else:
                for eclass, entities in extended_entities:
                    for index, entity in enumerate(entities):
                        if config.get('only_text_posts', False):
                            await self.sendMessage(
                                chat_id=channel,
                                text=text)
                        elif eclass == 'media':
                            # remove the url from the tweet text
                            text = text.replace(
                                entity['url'],
                                '')

                            if entity['type'] == 'photo':
                                url = entity['media_url_https']

                                photo_file = urllib.request.urlopen(url)
                                filename = url.split('/')[-1]

                                await self.sendPhoto(
                                    chat_id=channel,
                                    photo=(filename, photo_file),
                                    caption=text
                                )

                            elif entity['type'] == 'animated_gif':
                                pass

                            elif entity['type'] == 'video':
                                pass

                            else:
                                logger.warning('Unknown entity type: %s',
                                               entity)

            # save the last tweet processed
            self.pers['last_tweets'][subscription_id] = tweet.id

__bot__ = Twitter
