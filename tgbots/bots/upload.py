import telepot
import telepot.async

import aiohttp
import tgbots.util


class UploadException(Exception):
    pass


class Upload(tgbots.util.Bot):
    def __init__(self, config, persistance):
        super(Upload, self).__init__(config, persistance)

        self.session = aiohttp.ClientSession(headers={
            'Authorization': 'Client-ID %s' % self.config['client_id']
        })
        self.cache = {}

    async def upload_imgur(self, url):
        result = await self.session.post(
            'https://api.imgur.com/3/image',
            data={
                'image': url,
                'type': 'url'
            })

        json = await result.json()

        if json['status'] == 200:
            return json['data']['link']
        else:
            raise UploadException(json[''])

    async def on_chat_message(self, msg):
        content_type, chat_type, chat_id = telepot.glance(msg)

        # Only work on private chats
        if chat_type != 'private':
            return

        # command
        if content_type == 'text':
            if msg['text'].startswith('/'):
                args = msg['text'][1:].split(' ')
                cmd = args[0]

                if cmd == 'none':
                    pass

                else:
                    await self.sendMessage(
                        chat_id,
                        "Hi! \nSend me pictures or videos." +
                        "\nI'll upload them " +
                        "and send you an url!")

        elif content_type == 'photo':
            await self.sendChatAction(chat_id, 'upload_photo')

            photo = max(msg['photo'], key=lambda photo: photo['file_size'])
            tgfile = await self.getFile(photo['file_id'])
            # extension = tgfile['file_path'].split('.')[-1]

            tg_url = "https://api.telegram.org/file/bot%s/%s" % (
                self.config['key'],
                tgfile['file_path'])

            try:
                imgur_url = await self.upload_imgur(tg_url)
            except UploadException:
                await self.sendMessage(chat_id, 'An error ocurred.')
            else:
                await self.sendMessage(
                    chat_id,
                    imgur_url,
                    disable_web_page_preview=True)

        elif content_type == 'document':
            pass

__bot__ = Upload
