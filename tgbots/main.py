import importlib
import logging
import os
import sys

import yaml

import asyncio
import tgbots.util

logger = logging.getLogger(__name__)


def main():
    # get an event loop
    loop = asyncio.get_event_loop()

    # enable debug logging in the event loop
    loop.set_debug(True)

    os.makedirs('log', exist_ok=True)

    logFormatter = logging.Formatter("%(asctime)s %(levelname)-8s %(name)s  %(module)s:%(funcName)s:%(lineno)d - %(message)s ")

    main_logger = logging.getLogger()
    loop_logger = logging.getLogger('asyncio')

    main_logger.setLevel('DEBUG')
    loop_logger.setLevel('WARNING')

    main_fh = logging.FileHandler("log/main.log")
    loop_fh = logging.FileHandler("log/loop.log")
    sh = logging.StreamHandler()

    main_fh.setLevel('INFO')
    loop_fh.setLevel('WARNING')
    sh.setLevel('INFO')

    main_fh.setFormatter(logFormatter)
    loop_fh.setFormatter(logFormatter)
    sh.setFormatter(logFormatter)

    main_logger.addHandler(main_fh)
    main_logger.addHandler(sh)
    loop_logger.addHandler(loop_fh)

    # persistence directory
    os.makedirs('persistence', exist_ok=True)
    persistence = []

    try:
        configfile = sys.argv[1]
    except IndexError:
        print("Usage: ./%s <config.yaml>" % sys.argv[0])
        return

    with open(configfile) as f:
        config = yaml.load(f)

    for bot_type, config in config.items():
        if config.get('disabled', False) is True:
            continue

        logger.info("Loading %s...", bot_type)

        try:
            # get the bot class
            bot_cls = importlib.import_module('bots.%s' % bot_type).__bot__
        except ImportError as e:
            logger.critical("The bot %s couldn't be loaded - %s",
                            bot_type, e)
            return

        # create or load a persistence file for this bot
        p = tgbots.util.SyncDict('persistence/%s.json' % bot_type)
        persistence.append(p)

        # initialize the ebot
        bot_obj = bot_cls(config, p)

        # add the bot to the event loop
        loop.create_task(bot_obj.message_loop())

    try:
        # start the event loop
        loop.run_forever()
    except KeyboardInterrupt:
        print("Exiting")

        for pers in persistence:
            print("Saving %s" % pers)
            pers.save()

        exit()


if __name__ == '__main__':
    main()
