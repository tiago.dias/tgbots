import datetime
import sys
import glob
import json
import os
import shutil
import logging

logging.basicConfig(
    format="%(asctime)s %(levelname)-8s %(name)s  %(module)s:%(funcName)s:%(lineno)d - %(message)s ",
    level=logging.INFO)

class LogConverter(object):
    def __init__(self, log_dir):
        """Converts telegram messages to other formats"""
        self.log_dir = log_dir
        pass

    def contains(self, a, b):
        """Returns the element if any of the elements in a is in b"""
        for element in a:
            if element in b:
                return element
        return None

    def get_hostname_equivalent(self, user):
        hostname = str(user['id'])
        hostname += '@' + user.get('username', 'undefined')
        return hostname

    def get_name(self, user, real_name=False, with_phone=False):
        if not real_name:
            if 'username' in user:
                return user['username']

        name = user.get('first_name', '')
        if 'last_name' in user != '':
            name += ' ' + user['last_name']

        if with_phone:
            if 'phone_number' in user:
                name += ' (%s)' % user['phone_number']

        return name

    def location_to_coords(self, location):
        return "%d,%d" % (
            location['latitude'],
            location['longitude']
        )

    def largest_picture(self, pictures):
        return max(pictures, key=lambda photo: photo['file_size'])

    def sanitize(self, text):
        text = text.replace('\r', '')
        text = ' // '.join(text.split('\n'))
        return text

    def msg_to_energymech(self, msg, oldnicks):
        message = ""

        username = self.get_name(msg['from'])

        # check if the nick is new
        if username not in oldnicks:
            oldnicks.append(username)
            # twice so the nick tracker will use the first one on @mentions
            # (link both @ and @-less nicks)
            message += "*** %s is now known as @%s\n" % (
                username,
                username
            )
            message += "*** @%s is now known as %s\n" % (
                username,
                username
            )

        # prefix, with username and forward/reply marking
        prefix = "<%s>" % username

        if 'forward_from' in msg:
            prefix += ' (Forward from @%s)' % msg['forward_from'].get(
                'username', msg['from'].get('id', -1))

        if 'reply_to_message' in msg:
            prefix += ' (Reply to @%s)' % msg['reply_to_message']['from'].get(
                'username', msg['from'].get('id', -1))

        # text
        text = self.sanitize(msg.get('text', ''))

        # space after the text
        if text:
            message += '%s %s' % (prefix, text)

        # check if it's a file type message
        file_type = self.contains(
            msg,
            ['audio', 'document', 'photo', 'video', 'voice', 'sticker'])

        if file_type:
            if (isinstance(msg[file_type], list)):
                f = self.largest_picture(msg[file_type])
            else:
                f = msg[file_type]

            message += '%s %s:https://european.shitposting.agency/tg/%s' % (
                prefix, file_type, f['file_id'])

            if 'file_name' in f:
                message += '/%s' % f['file_name']

        elif 'contact' in msg:
            message += '%s Contact: %s' % (
                prefix,
                self.get_name(msg['contact'], real_name=True))

        elif 'venue' in msg:
            message += '%s Venue: %s, %s - %s' % (
                prefix,
                msg['venue']['title'],
                msg['venue']['address'],
                self.location_to_coords(msg['venue']['location']))

        elif 'location' in msg:
            message += '%s Location: %s' % (
                prefix,
                self.location_to_coords(msg['location']))

        elif 'new_chat_member' in msg:
            message += '*** Joins: %s (%s)' % (
                msg['new_chat_member'].get('username', -1),
                self.get_hostname_equivalent(msg['new_chat_member'])
            )

        elif 'left_chat_member' in msg:
            message += '*** Parts: %s (%s)' % (
                msg['left_chat_member'].get('username', -1),
                self.get_hostname_equivalent(msg['left_chat_member'])
            )

        elif 'new_chat_title' in msg:
            message += "*** %s changes topic to '%s'" % (
                username,
                msg['new_chat_title']
            )

        elif 'new_chat_photo' in msg:
            message += "*** %s changes photo to photo:%s" % (
                username,
                self.largest_picture(msg['new_chat_photo'])['file_id']
            )

        elif 'delete_chat_photo' in msg:
            message += "*** %s deletes channel photo" % (
                username
            )

        if 'caption' in msg:
            message += ' %s' % self.sanitize(msg['caption'])

        return message, oldnicks

    def convert_all(self, func, output_dir):
        shutil.rmtree(output_dir)

        for filename in glob.glob("%s/*.log" % self.log_dir):
            logging.info('Processing %s', filename)

            out_dir = os.path.join(
                output_dir, os.path.basename(filename).split('.')[0])
            os.makedirs(out_dir, exist_ok=True)

            with open(filename, 'r') as f:
                # keep track of what file we have opened, so we don't
                # need to keep opening and closing for each line
                file_obj = None
                old_nicks = []

                for line in f:
                    msg = json.loads(line)

                    date = datetime.datetime.fromtimestamp(msg['date'])
                    out_file = os.path.join(
                        out_dir,
                        '%s.log' % date.strftime("%Y-%m-%d"))

                    if (not file_obj) or file_obj.name != out_file:
                        if file_obj:
                            file_obj.close()
                        file_obj = open(out_file, 'w')

                    lines, old_nicks = func(msg, old_nicks)
                    for line in lines.split('\n'):
                        date = datetime.datetime.fromtimestamp(msg['date'])
                        file_obj.write("[%s] " % date.strftime("%H:%M:%S"))
                        file_obj.write(line + '\n')

if __name__ == '__main__':
    converter = LogConverter('../../log/channels')
    converter.convert_all(converter.msg_to_energymech, './irc')
