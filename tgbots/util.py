import functools
import json

import telepot
import telepot.async

import asyncio
import logging

logger = logging.getLogger(__name__)


class SyncDict(dict):
    def __init__(self, filename, *args):
        self.filename = filename

        try:
            with open(self.filename, 'r') as f:
                data = json.load(f)
        except:
            data = {}

        dict.__init__(self, data)

    def save(self):
        with open(self.filename, 'w') as f:
            json.dump(self, f, sort_keys=True, indent=2)


class Bot(telepot.async.Bot):
    def __init__(self, config, persistence):
        self.config = config
        self.pers = persistence

        super(Bot, self).__init__(config['key'])


async def run_async(func, *args, **kwargs):
    logger.debug('Running %s in an executor', func.__name__)
    loop = asyncio.get_event_loop()
    return await loop.run_in_executor(
        None, functools.partial(func, *args, **kwargs))


def username(user):
    return user.get('username', user['id'])
